from django.conf.urls import url, patterns


urlpatterns = patterns("",
    url(r'^$', "blogs.views.blog_index", name="home"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', "blogs.views.blog_post_detail", name="blog_post"),
    url(r'^post/(?P<post_pk>\d+)/$', "blogs.views.blog_post_detail", name="blog_post_pk"),
    url(r'^(?P<section_slug>[-\w]+)/$', "blogs.views.blog_section_list", name="blog_section"),
)